import Sound.ALSA.PCM
         (SoundFmt(SoundFmt), sampleFreq, soundSourceRead,
          SoundSource, alsaSoundSource, withSoundSource, )

import Foreign
import Data.Int (Int16, )

import Data.Vector
import Numeric.FFT.Vector.Unitary

bufSize :: Int
bufSize = 1000

inputFormat :: SoundFmt Int16
inputFormat = SoundFmt { sampleFreq = 8000 }


main :: IO ()
main = let source = alsaSoundSource "default" inputFormat
        in allocaArray     bufSize $ \buf  ->
           withSoundSource source  $ \from ->
               loop source from bufSize buf

-- | assumes that the file contains numbers in the host's byte order
loop :: SoundSource h Int16 -> h Int16 -> Int -> Ptr Int16 -> IO ()
loop src h n buf =
    do n' <- soundSourceRead src h (castPtr buf) n
       xs <- peekArray n buf
       let converted = fromList [fromIntegral x | x <- xs]
       let toto = run dct2 converted

       loop src h n buf
